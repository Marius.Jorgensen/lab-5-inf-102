package INF102.lab5.graph;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private Set<V> found;
    private Set<V> left;
    private TreeSet<V> path;
    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
        reset();
    }

    private void reset() {
        this.found = new HashSet<>();
        this.path = new TreeSet<>();
        this.left = new HashSet<>();
        for (V node : graph.getNodes()) {
            left.add(node);
        }
    }


    @Override
    public boolean connected(V u, V v) {    
        found.add(u);
        path.add(u);
        left.remove(u);
        if(u.equals(v)){
            reset();
            return true;
        }

        while (nextToFound(path.last())){ 
        if(path.size() == 1){
            reset();
            return false;
        }
        path.remove(path.last());
        }  

        Set<V> neighbours = graph.getNeighbourhood(path.last());
        for (V node : neighbours) {
            if (found.contains(node)) {
                continue;
            }
            return connected(node, v);
        }
    return false;

    }
    private boolean nextToFound(V node){
        for(V n : graph.getNeighbourhood(node)){
            if(!found.contains(n)){
                return false;
            }
        }
        return true;
    }

  
}

